## Quiz Show

This is a Repository for practice quiz questions using Spring boot, Angular and Mysql.


## Description
The Quiz Show Portal is an innovative platform designed for users to take exams and practice multiple-choice questions. Leveraging the power of Angular for the frontend, Spring Boot for the backend, and MySQL for data storage, this project provides a seamless and user-friendly experience for individuals preparing for interviews, exams, or quizzes.

Features:

- User-Friendly Interface:Intuitive design ensures a smooth and enjoyable user experience.
Responsive layout for seamless access on various devices.


- Multiple Choice Questions (MCQs):Users can take exams consisting of multiple-choice questions.
Questions cover a range of topics, making it suitable for various exam preparations.


- User Profiles and Progress Tracking:Personalized user profiles to track progress and performance.
Historical data and scores available for review.


- Exam Simulation:Simulate real exam conditions with timed quizzes.
Mimic different exam patterns and difficulty levels.


- Comprehensive Admin Panel:Admin dashboard for managing questions, exams, and user data.
Easily add, edit, or remove questions to keep content up-to-date.

- Background:The Quiz Show Portal aims to revolutionize the way individuals prepare for exams and interviews. With a focus on providing a comprehensive, interactive, and technology-driven learning experience, the platform caters to a diverse audience seeking effective and engaging exam preparation.

## Installation

- For Backend:-
1. Clone this repository.
2. Open the project in your favorite Java IDE.
3. Create a run Configurations.
4. Run project.
5. Hit endpoint-http://localhost:8080/user/

- For Frontend:-
1. Clone this repository.
2. Open the project in VS Code.
3. Run project- ng serve
4. Open your browser on http://localhost:4200/ 
